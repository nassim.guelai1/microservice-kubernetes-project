package com.example.msregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@EnableEurekaServer
@SpringBootApplication
public class MsRegistrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsRegistrationApplication.class, args);
    }

}
