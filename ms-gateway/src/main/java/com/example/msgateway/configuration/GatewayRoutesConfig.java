package com.example.msgateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
//public class GatewayRoutesConfig {
//
//    @Bean
//    RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
//        return builder.routes()
//                .route(r -> r.path("/users/**").uri("lb://MS-USER"))
//                .route(r -> r.path("/articles/**").uri("lb://MS-ARTICLE"))
//                .route(r -> r.path("/authors/**").uri("lb://MS-AUTHOR"))
//                .build();
//    }
//}