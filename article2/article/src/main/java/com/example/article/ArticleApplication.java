package com.example.article;

import com.example.article.entites.Article;
import com.example.article.repository.ArticleRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

//@EnableEurekaClient

@EnableDiscoveryClient
@SpringBootApplication
public class ArticleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticleApplication.class, args);
	}

	@Bean
	CommandLineRunner start(ArticleRepository articleRepository){
		return args ->{
			articleRepository.save(new Article(null, "Title 01", "Contain 01"));
			articleRepository.save(new Article(null, "Title 02", "Contain 02"));
			articleRepository.findAll().forEach(a->{
				System.out.println(a.getTitle());
			});
		};
	}
}
