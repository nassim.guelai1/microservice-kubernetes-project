package com.example.article.controller;

import com.example.article.entites.Article;
import com.example.article.repository.ArticleRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class ArticleController {

    private final ArticleRepository articleRepository;

    @GetMapping("/articles")
    public List<Article> list(){
        return articleRepository.findAll();
    }

    @GetMapping("/article/{id}")
    public Article getArticle(@PathVariable(required = false) Long id){
        return articleRepository.findById(id).get();
    }

    @PutMapping("/article/update/{id}")
    public Article update(@PathVariable(required = false) Long id, Article article){
        article.setId(id);
        return articleRepository.save(article);
    }

    @PostMapping("/article/add")
    public Article save(Article article){
        return articleRepository.save(article);
    }

    @DeleteMapping("/article/delete/{id}")
    public void delete(@PathVariable(required = false) Long id){
        articleRepository.deleteById(id);
    }
}
